FROM java:8-jre
MAINTAINER Xawirses <xawirses@gmail.com>
LABEL maintainer="Xawirses <xawirses@gmail.com>"

ENV MINECRAFT_VERSION 1.10.2
ENV MINECRAFT_JAR minecraft_server.${MINECRAFT_VERSION}.jar
ENV MINECRAFT_URL https://s3.amazonaws.com/Minecraft.Download/versions/${MINECRAFT_VERSION}/${MINECRAFT_JAR}

RUN mkdir /minecraft
WORKDIR /minecraft

RUN curl --create-dirs -sLo /minecraft/${MINECRAFT_JAR} ${MINECRAFT_URL}
RUN echo "eula=true" > /minecraft/eula.txt

COPY entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod +x /usr/bin/entrypoint.sh

ENV JAVA_RAM 4G
ENV JAVA_OPTS -XX:+UseG1GC -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M

EXPOSE 25565

ENTRYPOINT ["/usr/bin/entrypoint.sh"]